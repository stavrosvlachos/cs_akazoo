package com.example.estelle.cs_akazoo_app.playlists;

import java.util.ArrayList;

public class PlaylistsPreseneterImpl implements PlaylistsPresenter{

    PlaylistsView playlistsView;

    public PlaylistsPreseneterImpl(PlaylistsView playlistsView) {
        this.playlistsView = playlistsView;


    }

    @Override
    public void getPlaylists() {
        ArrayList playlists = getMokedPlayLists();

        playlistsView.showPlaylists(playlists);
        }






    private ArrayList getMokedPlayLists() {

        ArrayList<Playlist> playlists = new ArrayList<>();
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 12));
        playlists.add(new Playlist("3", "Nisiotika", 7));
        playlists.add(new Playlist("4", "Rock", 2));
        playlists.add(new Playlist("5", "Nisiotika", 11));
        playlists.add(new Playlist("6", "Rock", 12));
        playlists.add(new Playlist("7", "Nisiotika", 11));
        playlists.add(new Playlist("8", "Rock", 12));
        playlists.add(new Playlist("9", "Nisiotika", 11));
        playlists.add(new Playlist("10", "Rock", 12));
        playlists.add(new Playlist("11", "Nisiotika", 11));
        playlists.add(new Playlist("12", "Rock", 12));

        return playlists;
    }
}
