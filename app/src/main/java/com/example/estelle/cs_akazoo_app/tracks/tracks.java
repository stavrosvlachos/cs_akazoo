package com.example.estelle.cs_akazoo_app.tracks;

public class tracks {
    private String trackName;
    private String trackArtist;
    private String trackCategory;
    //private String trackLogoUrl;

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public void setTrackArtist(String trackArtist) {
        this.trackArtist = trackArtist;
    }

    public String getTrackCategory() {
        return trackCategory;
    }

    public void setTrackCategory(String trackCategory) {
        this.trackCategory = trackCategory;
    }

    //public String getTrackLogoUrl() {
    //    return trackLogoUrl;
    //}

    //public void setTrackLogoUrl(String trackLogoUrl) {
    //    this.trackLogoUrl = trackLogoUrl;
    //}

    public tracks(String trackName, String trackArtist, String trackCategory) {

        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackCategory = trackCategory;
        //this.trackLogoUrl = trackLogoUrl;
    }
}
