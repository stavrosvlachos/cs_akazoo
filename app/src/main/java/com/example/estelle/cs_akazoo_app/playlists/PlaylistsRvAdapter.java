package com.example.estelle.cs_akazoo_app.playlists;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;

import java.util.ArrayList;

public class PlaylistsRvAdapter extends RecyclerView.Adapter<PlaylistsRvAdapter.PlaylistsViewHolder> {

    private ArrayList<Playlist> playlists;
    private onPlaylistClickListener listener;


    public PlaylistsRvAdapter(ArrayList<Playlist> playlists, onPlaylistClickListener listener) {
        this.playlists = playlists;
        this.listener = listener;
    }



    //public PlaylistsRvAdapter(ArrayList<Playlist> playlists) {
    //    this.playlists = playlists;
    //
    //
    //}

    public static class PlaylistsViewHolder extends RecyclerView.ViewHolder {
        TextView mPlaylistName;
        TextView mTracksNumber;
        ImageView mPlaylistLogo;
        RelativeLayout mPlaylistItemRoot;


        public PlaylistsViewHolder(View v) {
            super(v);

            //itemView.setOnClickListener(new View.OnClickListener() {
            //public CharSequence a;

            //@Override
            //public void onClick(View view) {

            // a = ((AppCompatTextView) ((PlaylistsViewHolder) this.this$0).mPlaylistName).getText();
            //   a = this.clone(mPlaylistName.getText()));

            //Log.d("Playlists1", "");
            //}
            //});

            mPlaylistName      = v.findViewById(R.id.playlists_name);
            mTracksNumber      = v.findViewById(R.id.tracks_number);
            mPlaylistLogo      = v.findViewById(R.id.playlist_logo);
            mPlaylistItemRoot  = v.findViewById(R.id.PlayListItem_root);

        }

        ;
    }

    ;


    @NonNull
    @Override
    public PlaylistsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_playlist_item, viewGroup, false);


        PlaylistsViewHolder vh = new PlaylistsViewHolder(v);

        return vh;
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistsViewHolder viewHolder, int i) {
        final int position = i;

        viewHolder.mPlaylistName.setText(playlists.get(position).getName());
        viewHolder.mTracksNumber.setText(String.valueOf(playlists.get(position).getItemCount()));
        viewHolder.mPlaylistLogo.setImageResource(R.mipmap.ic_launcher);
        viewHolder.mPlaylistItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPlaylistClicked(playlists.get(position));

            }


        });



    }
}
