package com.example.estelle.cs_akazoo_app.tracks;


import java.util.ArrayList;

public class TracklistsPreseneterImpl implements TracklistsPresenter{

    TracklistsView tracklistsView;

    public TracklistsPreseneterImpl(TracklistsView tracklistsView) {
        this.tracklistsView = tracklistsView;


    }

    @Override
    public void getTrackslists() {

        ArrayList tracks = getMockTracks();

        tracklistsView.showTracklists(tracks);

    }

    // Routine to add Mock data
    private ArrayList getMockTracks() {

        ArrayList<tracks> tracks = new ArrayList<tracks>();

        for (int i = 1; i < 100; i++) {
            tracks track = new tracks("Track name" + " " + i, "Track Artist"
                    + " " + i, "Track category" + " " + i);
            tracks.add(track);



        };
        return tracks;
    };
}
