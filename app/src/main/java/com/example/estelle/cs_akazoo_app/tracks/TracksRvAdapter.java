package com.example.estelle.cs_akazoo_app.tracks;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.tracks.tracks;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class TracksRvAdapter extends RecyclerView.Adapter<TracksRvAdapter.TracksViewHolder> {

    // Array
    private ArrayList<tracks> tracks;

    public TracksRvAdapter(ArrayList<tracks> tracks) {
        this.tracks = tracks;
    }

    public TrackRvAdapter(ArrayList<tracks> tracks, onTrackClickListener listener) {
        this.tracks = tracks;
        this.listener = listener;
    }



    // Constructor
    public static class TracksViewHolder extends RecyclerView.ViewHolder {
        TextView  mTrackName;
        TextView  mTrackArtist;
        TextView  mTrackCategory;
        ImageView mTrackLogo;
        RelativeLayout mTracklistItemRoot;


        public TracksViewHolder(View v) {
            super(v);
            mTrackName     = v.findViewById(R.id.track_name);
            mTrackArtist   = v.findViewById(R.id.track_artist);
            mTrackLogo     = v.findViewById(R.id.track_logo);
            mTrackCategory = v.findViewById(R.id.track_category);
            mTracklistItemRoot  = v.findViewById(R.id.TrackListItem_root);
        }
    }

    // Event View Holder
    @NonNull
    @Override
    public TracksViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_track_item ,viewGroup, false);

        TracksViewHolder vh = new TracksViewHolder(v);

        return vh;
    }

    // Must do it
    @Override
    public int getItemCount() {
        return tracks.size();

    }

    // Must do it
    @Override
    public void onBindViewHolder(@NonNull TracksViewHolder viewHolder, int i) {
        final int position = 1;

        viewHolder.mTrackName.setText(tracks.get(position).getTrackName());
        viewHolder.mTrackArtist.setText(tracks.get(position).getTrackArtist());
        viewHolder.mTrackCategory.setText(tracks.get(position).getTrackCategory());
        viewHolder.mTrackLogo.setImageResource(R.mipmap.ic_launcher);

        viewHolder.mTracklistItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPlaylistClicked(tracks.get(position));








    }
}
