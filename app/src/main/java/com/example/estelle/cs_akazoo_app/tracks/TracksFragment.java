package com.example.estelle.cs_akazoo_app.tracks;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;



import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */


public class TracksFragment extends Fragment implements TracklistsView {

    RecyclerView mTracksRv;
    TracklistsPresenter presenter;

    public TracksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tracks, container, false);

        mTracksRv = v.findViewById(R.id.tracks_rv);

        // Display items
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        //GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);

        mTracksRv.setLayoutManager(layoutManager);

        mTracksRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        //mTracksRv.setLayoutManager(new GridLayoutManager(getActivity(),1));

        // Load of Array with Mock routine
        //ArrayList<tracks> tracks = new ArrayList<>();
        //getMockTracks(tracks);

        //tracks.add(new tracks("Track name 1", "Track Artist 1", "Track category 1"));

        presenter  = new TracklistsPreseneterImpl(this);
        presenter.getTrackslists();

        return v;


    }


    @Override
    public void showTracklists(final ArrayList<tracks> tracklists) {

        TracksRvAdapter TracksRvAdapter = new TracksRvAdapter(tracklists) {
            @Override
            public void onPlaylistClicked(tracks tracks) {
                Log.d("CS_TAG", "The playlist " + tracks.getTrackName() + " got clicked ");

                Toast.makeText(getActivity(), "The playlist " + tracks.getTrackName() + " got clicked ",
                        Toast.LENGTH_LONG).show();

            };
        };

        mTracksRv.setAdapter(TracksRvAdapter);

    }






}
